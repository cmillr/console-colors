Console-Colors
==============

Simple: to set your console colors to the nice, new defaults, just run
the Console-Colors.reg registry file.  If you hate the new color scheme
and just want to reset your console colors back to the defaults, then 
run the Console-Defaults.reg file.

If you run the Console-Fonts.reg file, it will set your console window 
size to 115 chars wide by 52 lines high, and will change your font to 
16pt Consolas. This is just right for my screen, but it may be too big
for yours (but it's a good baseline either way.)

At present, Console-Defaults does not revert the Console-Fonts settings.

That's all there is to it!
